import axiosInstance from "../../plugins/axios";

export default {
  async getListPostPaging({ commit }, { pagesize = 3, currentPage = 1 }) {
    try {
      const result = await axiosInstance.get(
        `/post/getListPagination.php?pagesize=${pagesize}&currPage=${currentPage}`
      );
      commit("SET_LIST_POST", result.data.posts);
    } catch (error) {
      console.log("error", error);
    }
  },

  async getPostByCategory(
    { commit },
    { pagesize = 3, currentPage = 1, tagIndex = 1 }
  ) {
    try {
      const result = await axiosInstance.get(
        `/post/getListByCategory.php?pagesize=${pagesize}&currPage=${currentPage}&tagIndex=${tagIndex}`
      );
      console.log(result);
      commit;
    } catch (error) {
      console.log(error);
    }
  },
};
